package co.edu.uniquindio.grid.pruebas;

import java.util.Date;

import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.persistence.UsingDataSet;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import co.edu.uniquindio.grid.entidades.Empleado;
import co.edu.uniquindio.grid.entidades.Persona;
import co.edu.uniquindio.grid.exceptiones.InformacionRepetidaException;
import co.edu.uniquindio.grid.negocio.AdministradorEJB;

/**
 * permite probar la la capa de negocio
 * @author EinerZG
 * @version 1.0
 */
@RunWith(Arquillian.class)
public class TestNegocio {

	/**
	 * permite realizar las trabasaciones con la base d datos 
	 */
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * permite inicializar una instancia del ejb de administrador
	 */
	@EJB
	private AdministradorEJB administradorEJB;

	/**
	 * general los archivo de pruebas
	 * @return
	 */
	@Deployment
	public static Archive<?> createDeploymentPackage() {
		return ShrinkWrap.create(JavaArchive.class).addClass(AdministradorEJB.class)
				.addPackage(Persona.class.getPackage())
				.addAsResource("persistenceForTest.xml", "META-INF/persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	/**
	 * permite probar el agregar un empleado en la capa de negocio
	 */
	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet("persona.json")
	public void agregarEmpeladoTest() {

		try {

			Empleado empleado = new Empleado();
			empleado.setCedula("12345");
			empleado.setNombre("a");
			empleado.setApellido("a");
			empleado.setClave("12345");
			empleado.setEmail("pperez@mail.com");
			empleado.setFechaNacimiento(new Date());
			empleado.setSalario(23000000);

			Assert.assertNotNull(administradorEJB.agregarEmpleado(empleado));

		} catch (InformacionRepetidaException e) {
			Assert.fail("Información repetida: " + e.getMessage());
		} catch (Exception e) {
			Assert.fail("Otro erro repetida: " + e.getMessage());
		}

	}

}

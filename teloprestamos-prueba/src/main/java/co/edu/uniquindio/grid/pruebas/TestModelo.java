package co.edu.uniquindio.grid.pruebas;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.persistence.UsingDataSet;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import co.edu.uniquindio.grid.entidades.Administrador;
import co.edu.uniquindio.grid.entidades.Cliente;
import co.edu.uniquindio.grid.entidades.Empleado;
import co.edu.uniquindio.grid.entidades.Persona;

/**
 * clase encargada de generar las tablas del modelo generado en la capa de
 * persistensia
 * 
 * @author EinerZG
 * @version 1.0 12/08/2018
 */
@RunWith(Arquillian.class)
public class TestModelo {

	@PersistenceContext
	private EntityManager entityManager;

	@Deployment
	public static Archive<?> createTestArchive() {
		return ShrinkWrap.create(WebArchive.class, "test.war").addPackage(Persona.class.getPackage())
				.addAsResource("persistenceForTest.xml", "META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");

	}

	/**
	 * permite buscar un cliente con base a su cedula
	 */
	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet("persona.json")
	public void buscarClienteTest() {

		Cliente cliente = entityManager.find(Cliente.class, "123456789");
		Assert.assertNotNull(cliente);

	}

	/**
	 * permite realizar la eliminacion de prueba de un administrador
	 */
	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet("persona.json")
	public void eliminarAdministradorTest() {
		
		Administrador admin = entityManager.find(Administrador.class, "5678912345");
		entityManager.remove(admin);
		
		admin = entityManager.find(Administrador.class, "5678912345");
		Assert.assertNull(admin);
		
	}
	
	/**
	 * permite probar la transacción de agregar emeplado
	 */
	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet("persona.json")
	public void agregarEmpleadoTest() {
	
		Empleado empleado = new Empleado();
		empleado.setCedula("109445678");
		empleado.setNombre("Denilson");
		empleado.setApellido("De la torre");
		empleado.setClave("12345");
		empleado.setEmail("dagudelo@uqvirtual.edu.co");
		empleado.setFechaNacimiento(new Date());
		empleado.setSalario(200000000);
		
		entityManager.persist(empleado);
		
		Empleado empleado1 = entityManager.find(Empleado.class, empleado.getCedula());
		Assert.assertNotNull(empleado1);

		
	}
	
	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({"persona.json","prestamo.json"})
	public void buscarClientePrestamoTest() {
		
		Cliente cliente = entityManager.find(Cliente.class, "123456789");
		Assert.assertEquals(cliente.getPrestamos().size(), 1);
		
	}
	
	/**
	 * permite listar todas las personas clientes del banco
	 */
	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({"persona.json","prestamo.json"})
	public void listarTodosLosClienteTest() {
		
		TypedQuery<Empleado> query = entityManager.createQuery("select e from Empleado e",Empleado.class);
		List<Empleado> personas = query.getResultList();
		
		Assert.assertEquals(personas.size(), 2);
		
	}
	
	/**
	 * Permite obtener un cliente según la credenciales
	 */
	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({"persona.json","prestamo.json"})
	public void obtenerClientePorCredencialesTest() {
		
		try {
			TypedQuery<Persona> query = entityManager.createNamedQuery(Persona.PERSONA_POR_CREDENCIALES, Persona.class);
			query.setParameter("cedula", "567891");
			query.setParameter("clave1", "12345");
			Persona p = query.getSingleResult();
			
			Assert.assertEquals("Yeso", p.getNombre());
		}
		catch (NoResultException e) {
			Assert.fail(String.format("Error al buscar el cliente: %s", e.getMessage()));
		}
		
	}
	
	
	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({"persona.json"})
	public void cambiarDTypeTest() {
		
		Persona persona = entityManager.find(Cliente.class, "123456789");
		
		
		System.out.println(persona);
		
	}
	
	@Test
	public void generarTest() {

	}

}

package co.edu.uniquindio.grid.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.annotation.FacesConfig;
import javax.faces.annotation.FacesConfig.Version;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;

import co.edu.uniquindio.grid.entidades.Cliente;
import co.edu.uniquindio.grid.entidades.Persona;
import co.edu.uniquindio.grid.exceptiones.InformacionRepetidaException;
import co.edu.uniquindio.grid.negocio.AdministradorEJB;

@Named(value = "clienteBean")
@FacesConfig(version = Version.JSF_2_3)
@ApplicationScoped
public class ClienteBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * cedula del cliente
	 */
	@NotNull(message = "Ingrese el número de celula")
	private String cedula;
	/**
	 * nombre de la persona
	 */
	private String nombre;
	/**
	 * apellido de la persona
	 */
	@NotNull
	private String apellido;
	/**
	 * email de la persona
	 */
	@NotNull
	@Pattern(regexp = "[a-zA-Z0-9]+@[a-zA-Z0-9]+\\.[a-zA-Z0-9]+", message = "Por favor ingrese un correo correcto")
	private String email;
	/**
	 * clave de acceso a la plataforma
	 */
	@NotNull(message = "Ingrese una clave")
	private String clave;

	/**
	 * clave de conformacion
	 */
	private String claveConfir;

	/**
	 * fecha de nacimiento de la persona
	 */
	@Past
	private Date fecha;

	/**
	 * lista de clientes del banco
	 */
	private List<Persona> clientes;

	/**
	 * cliente a selecionar
	 */
	private Persona cliente;

	@EJB
	private AdministradorEJB administradorEJB;

	/**
	 * permite registrar un cliente
	 */
	public String registrar() {

		if (clave.equals(claveConfir)) {

			Cliente cliente = new Cliente();

			cliente.setCedula(cedula);
			cliente.setApellido(apellido);
			cliente.setNombre(nombre);
			cliente.setClave(clave);
			cliente.setEmail(email);
			cliente.setFechaNacimiento(fecha);

			try {

				administradorEJB.agregarCliente(cliente);
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Registro exitoso",
						"Registro exitoso");
				FacesContext.getCurrentInstance().addMessage(null, facesMsg);

				cedula = "";
				apellido = "";
				nombre = "";
				clave = "";
				email = "";
				fecha = null;

				return "/admin/cliente/clientes";

			} catch (InformacionRepetidaException e) {
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage());
				FacesContext.getCurrentInstance().addMessage(null, facesMsg);

			}

		} else {

			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Las claves agregadas deben coincidir",
					"Las claves agregadas deben coincidir");
			FacesContext.getCurrentInstance().addMessage(null, facesMsg);

		}

		return null;

	}

	
	/**
	 * permite inactiva a un cliente
	 */
	public String eliminarCliente() {		
		if( cliente != null ) {
			try {
				administradorEJB.inactivarCliente(cliente);
				return "/admin/cliente/clientes";
			} catch (Exception e) {
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage());
				FacesContext.getCurrentInstance().addMessage(null, facesMsg);
			}
		}
		return null;
	}
	
	
	/**
	 * @return the cliente
	 */
	public Persona getCliente() {
		return cliente;
	}

	/**
	 * @param cliente the cliente to set
	 */
	public void setCliente(Persona cliente) {
		this.cliente = cliente;
	}

	/**
	 * @return the clientes
	 */
	public List<Persona> getClientes() {
		clientes = administradorEJB.listarClienteActivos();
		return clientes;
	}

	/**
	 * @param clientes the clientes to set
	 */
	public void setClientes(List<Persona> clientes) {
		this.clientes = clientes;
	}

	/**
	 * @return the cedula
	 */
	public String getCedula() {
		return cedula;
	}

	/**
	 * @param cedula the cedula to set
	 */
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the apellido
	 */
	public String getApellido() {
		return apellido;
	}

	/**
	 * @param apellido the apellido to set
	 */
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the clave
	 */
	public String getClave() {
		return clave;
	}

	/**
	 * @param clave the clave to set
	 */
	public void setClave(String clave) {
		this.clave = clave;
	}

	/**
	 * @return the claveConfir
	 */
	public String getClaveConfir() {
		return claveConfir;
	}

	/**
	 * @param claveConfir the claveConfir to set
	 */
	public void setClaveConfir(String claveConfir) {
		this.claveConfir = claveConfir;
	}

	/**
	 * @return the fecha
	 */
	public Date getFecha() {
		return fecha;
	}

	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

}

/**
 * 
 */
package co.edu.uniquindio.grid.bean;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.annotation.FacesConfig;
import javax.faces.annotation.FacesConfig.Version;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import co.edu.uniquindio.grid.entidades.Administrador;
import co.edu.uniquindio.grid.entidades.Persona;
import co.edu.uniquindio.grid.negocio.AdministradorEJB;

/**
 * ManageBean que permite manejar el inicio de sesion
 * 
 * @author EinerZG
 * @version 1.0
 */
@FacesConfig(version = Version.JSF_2_3)
@Named("seguridadBean")
@ApplicationScoped
public class SeguridadBean{

	/**
	 * persona que se desea autenticar
	 */
	private Persona usuario;
	/**
	 * estado de la autenticacion
	 */
	private boolean autenticado;
	/**
	 * EJB con las operaciones de admin
	 */
	@EJB
	private AdministradorEJB administradorEJB;

	@PostConstruct
	private void inicializar() {
		usuario = new Persona();
	}

	/**
	 * permite loguea a un usuario segun su cedula y clave
	 * 
	 * @return navegación al menuinical o permanecer en el login
	 */
	public String loguin() {
		try {
			
			Persona u = administradorEJB.buscarUsuarioPorCredenciales(usuario.getCedula(), usuario.getClave());
			
			if ( u != null ) {
				
				if( u instanceof Administrador ) {
					usuario = u;
					autenticado = true;
					FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Inicio de sesión exitoso",
							"Inicio de sesión exitoso");
					FacesContext.getCurrentInstance().addMessage(null, facesMsg);
					return "/index";					
				}
				else {
					FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Solo los administradores pueden ingresar a la aplicacion",
							"Solo los administradores pueden ingresar a la aplicacion");
					FacesContext.getCurrentInstance().addMessage(null, facesMsg);
				}
				
			}
			
		} catch (Exception e) {
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage());
			FacesContext.getCurrentInstance().addMessage(null, facesMsg);
		}
		return null;
	}

	/**
	 * permite cerrar la sesion del administrador
	 * @return pagina inicial
	 */
	public String cerrarSesion() {
		
		if( autenticado ) {
			autenticado = false;
			usuario = new Persona();
			return "/index";
		}
		return null;
	}
	
	public String index() {
		return "/index";
	}
		
	/**
	 * @return the usuario
	 */
	public Persona getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(Persona usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the autenticado
	 */
	public boolean isAutenticado() {
		return autenticado;
	}

	/**
	 * @param autenticado the autenticado to set
	 */
	public void setAutenticado(boolean autenticado) {
		this.autenticado = autenticado;
	}

}

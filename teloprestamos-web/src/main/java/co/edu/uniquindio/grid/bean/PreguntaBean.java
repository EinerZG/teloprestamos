package co.edu.uniquindio.grid.bean;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import co.edu.uniquindio.grid.entidades.Empleado;
import co.edu.uniquindio.grid.entidades.Pregunta;
import co.edu.uniquindio.grid.negocio.AdministradorEJB;
import co.edu.uniquindio.grid.negocio.ClienteEJB;

@Named(value = "preguntaBean")
@ApplicationScoped
public class PreguntaBean {

	private Date fecha;
	private String pregunta;
	private String cedulaCliente;
	private Empleado empleado;
	private List<Empleado> empleados;
	private List<Pregunta> preguntas;
	@EJB
	private ClienteEJB clienteEJB;
	@EJB
	private AdministradorEJB administradorEJB;
	
	@PostConstruct
	public void inicializar(){
	    empleados = administradorEJB.listarEmpleados();
	}

	public String realizarPregunta() {

		try {

			Pregunta pregunta = new Pregunta();
			pregunta.setFecha(fecha);
			pregunta.setPregunta(this.pregunta);
			pregunta.setCliente(clienteEJB.buscarCliente(cedulaCliente));
			
			clienteEJB.realizarPregunta(pregunta);
			
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Registro exitoso",
					"Registro exitoso");
			FacesContext.getCurrentInstance().addMessage(null, facesMsg);

			return "/info_pregunta";

		} catch (Exception e) {
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage());
			FacesContext.getCurrentInstance().addMessage(null, facesMsg);
			return null;
		}

	}
	
	/**
	 * @return the preguntas
	 */
	public List<Pregunta> getPreguntas() {
		return administradorEJB.listarPreguntas();
	}

	/**
	 * @param preguntas the preguntas to set
	 */
	public void setPreguntas(List<Pregunta> preguntas) {
		this.preguntas = preguntas;
	}

	/**
	 * @return the empleado
	 */
	public Empleado getEmpleado() {
		return empleado;
	}

	/**
	 * @param empleado the empleado to set
	 */
	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}

	/**
	 * @return the empleados
	 */
	public List<Empleado> getEmpleados() {
		return empleados;
	}

	/**
	 * @param empleados the empleados to set
	 */
	public void setEmpleados(List<Empleado> empleados) {
		this.empleados = empleados;
	}

	/**
	 * @return the fecha
	 */
	public Date getFecha() {
		return fecha;
	}

	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the pregunta
	 */
	public String getPregunta() {
		return pregunta;
	}

	/**
	 * @param pregunta the pregunta to set
	 */
	public void setPregunta(String pregunta) {
		this.pregunta = pregunta;
	}

	/**
	 * @return the cedulaCliente
	 */
	public String getCedulaCliente() {
		return cedulaCliente;
	}

	/**
	 * @param cedulaCliente the cedulaCliente to set
	 */
	public void setCedulaCliente(String cedulaCliente) {
		this.cedulaCliente = cedulaCliente;
	}

}

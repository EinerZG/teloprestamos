package co.edu.uniquindio.grid.bean;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.annotation.FacesConfig;
import javax.faces.annotation.FacesConfig.Version;
import javax.faces.annotation.ManagedProperty;
import javax.inject.Inject;
import javax.inject.Named;

import co.edu.uniquindio.grid.entidades.Persona;

@FacesConfig(
        // Activates CDI build-in beans
        version = Version.JSF_2_3
)
@Named(value = "indexBean")
@ApplicationScoped
public class IndexBean {
	
	@Inject @ManagedProperty(value="#{seguridadBean.usuario}")
	private Persona usuario;
	
	public String getMensaje () {
		return "Hola JSF!!";
	}

	/**
	 * @return the usuario
	 */
	public Persona getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(Persona usuario) {
		this.usuario = usuario;
	}
	
	

}

package co.edu.uniquindio.grid.util;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import co.edu.uniquindio.grid.entidades.Empleado;
import co.edu.uniquindio.grid.negocio.AdministradorEJBRemote;

@FacesConverter(value = "empleadoConverter")
public class EmpleadoConverter implements Converter<Empleado> {

	private AdministradorEJBRemote administradorEJB;

	public EmpleadoConverter() {

		try {
			administradorEJB = (AdministradorEJBRemote) new InitialContext().lookup(AdministradorEJBRemote.JNDI);
		} catch (NamingException e) {
			e.printStackTrace();
		}

	}

	@Override
	public Empleado getAsObject(FacesContext context, UIComponent component, String value) {
		Empleado empleado = null;
		if (value != null && !"".equals(value)) {
			try {
				empleado = administradorEJB.buscarEmpleado(value);
			} catch (Exception e) {
				throw new ConverterException(new FacesMessage(component.getClientId() + ":ID no valido"));
			}
		}
		return empleado;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Empleado empleado) {
		if (empleado != null)
			return String.format("%s", empleado.getCedula());
		return null;
	}

}

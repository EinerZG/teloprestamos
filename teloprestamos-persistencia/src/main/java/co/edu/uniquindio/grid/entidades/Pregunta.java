package co.edu.uniquindio.grid.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

/**
 * encapsula la informacion referente a la pregunta que hace cada cliente
 * @author EinerZG
 * @version 1.0 12/08/2018
 */
@Entity
@NamedQueries({@NamedQuery(name = Pregunta.LISTAR_PREGUNTAS, query= "select p from Pregunta p")})
public class Pregunta implements Serializable {
	   
	private static final long serialVersionUID = 1L;
	public static final String LISTAR_PREGUNTAS = "ListarPreguntas";
	
	/**
	 * identificado de la pregunta
	 */
	@Id
	@GeneratedValue
	private int id;
	/**
	 * fecha en que se realiza la pregunta
	 */
	private Date fecha;
	/**
	 * pregunta que se desea realizar
	 */
	private String pregunta;
	/**
	 * cliente que realizo la pregunta
	 */
	@ManyToOne
	private Cliente cliente;
	/**
	 * aistencia que 
	 */
	@OneToOne(mappedBy="pregunta")
	private Respuesta respuesta;

	public Pregunta() {
		super();
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the fecha
	 */
	public Date getFecha() {
		return fecha;
	}

	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the cliente
	 */
	public Cliente getCliente() {
		return cliente;
	}

	/**
	 * @param cliente the cliente to set
	 */
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	/**
	 * @return the respuesta
	 */
	public Respuesta getRespuesta() {
		return respuesta;
	}

	/**
	 * @param respuesta the respuesta to set
	 */
	public void setRespuesta(Respuesta respuesta) {
		this.respuesta = respuesta;
	}

	/**
	 * @return the pregunta
	 */
	public String getPregunta() {
		return pregunta;
	}

	/**
	 * @param pregunta the pregunta to set
	 */
	public void setPregunta(String pregunta) {
		this.pregunta = pregunta;
	}

	
	
}

package co.edu.uniquindio.grid.entidades;

import java.io.Serializable;
import java.lang.String;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Informacion basica de cada una de las personas asociadas a la entidad
 * bancaria
 * 
 * @author EinerZG
 * @version 1.0 12/08/2018
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@NamedQueries({
		@NamedQuery(name = Persona.PERSONA_POR_CREDENCIALES, query = "select p from Persona p where p.cedula=:cedula and p.clave=:clave"),
		@NamedQuery(name = Persona.LISTAR_TODOS, query = "select p from Persona p"),
		@NamedQuery(name = Persona.PERSONA_POR_EMAIL, query = "select p from Persona p where p.email=:email"),
		@NamedQuery(name = Persona.LISTAR_CLIENTES_ACTIVOS, query = "select p from Persona p where p.estado=:estado")})
public class Persona implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 * referencia para listar clientes activos
	 */
	public static final String LISTAR_CLIENTES_ACTIVOS = "ListarCLientesActivos";
	/**
	 * referencia para listar los clientes
	 */
	public static final String LISTAR_TODOS = "ListarLosClientes";
	/**
	 * referencias permite obtener las personas por credenciales 
	 */
	public static final String PERSONA_POR_CREDENCIALES = "PersonasPorCredenciales";
	/**
	 * referencia para obtener personas por email
	 */
	public static final String PERSONA_POR_EMAIL = "PersonaPorEmail";

	/**
	 * cedula de la persona
	 */
	@Id
	private String cedula;
	/**
	 * nombre de la persona
	 */
	private String nombre;
	/**
	 * apellido de la persona
	 */
	private String apellido;
	/**
	 * email de la persona
	 */
	private String email;
	/**
	 * clave de acceso a la plataforma
	 */
	private String clave;
	/**
	 * estado activo o inactivo
	 */
	private String estado;	
	/**
	 * fecha de nacimiento de la persona
	 */
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaNacimiento;

	/**
	 * listado de prestamos de la persona
	 */
	@OneToMany(mappedBy = "cliente")
	private List<Prestamo> prestamos;

	public Persona() {
		super();
	}

	/**
	 * @return the cedula
	 */
	public String getCedula() {
		return cedula;
	}

	/**
	 * @param cedula the cedula to set
	 */
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the apellido
	 */
	public String getApellido() {
		return apellido;
	}

	/**
	 * @param apellido the apellido to set
	 */
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the clave
	 */
	public String getClave() {
		return clave;
	}

	/**
	 * @param clave the clave to set
	 */
	public void setClave(String clave) {
		this.clave = clave;
	}

	/**
	 * @return the prestamos
	 */
	public List<Prestamo> getPrestamos() {
		return prestamos;
	}

	/**
	 * @param prestamos the prestamos to set
	 */
	public void setPrestamos(List<Prestamo> prestamos) {
		this.prestamos = prestamos;
	}

	/**
	 * @return the fechaNacimiento
	 */
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	/**
	 * @param fechaNacimiento the fechaNacimiento to set
	 */
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	
	

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cedula == null) ? 0 : cedula.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Persona other = (Persona) obj;
		if (cedula == null) {
			if (other.cedula != null)
				return false;
		} else if (!cedula.equals(other.cedula))
			return false;
		return true;
	}

}

package co.edu.uniquindio.grid.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * Cuota que se debe pagar por el prestamo realizado
 * @author EinerZG
 * @version 1.0 12/08/2018
 */
@Entity
public class Cuota implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 * identificado de la cuota
	 */
	@Id
	@GeneratedValue
	private int id;
	/**
	 * fecha en que se paga la cuota
	 */
	private Date fecha;
	/**
	 * pago que se realiza
	 */
	private double pago;
	/**
	 * prestamo asociado a la cuota
	 */
	@ManyToOne
	private Prestamo prestamo;

	public Cuota() {
		super();
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the fecha
	 */
	public Date getFecha() {
		return fecha;
	}

	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the pago
	 */
	public double getPago() {
		return pago;
	}

	/**
	 * @param pago the pago to set
	 */
	public void setPago(double pago) {
		this.pago = pago;
	}

	/**
	 * @return the prestamo
	 */
	public Prestamo getPrestamo() {
		return prestamo;
	}

	/**
	 * @param prestamo the prestamo to set
	 */
	public void setPrestamo(Prestamo prestamo) {
		this.prestamo = prestamo;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cuota other = (Cuota) obj;
		if (id != other.id)
			return false;
		return true;
	}  
	
   
}

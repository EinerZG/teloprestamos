package co.edu.uniquindio.grid.entidades;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

/**
 * Informacion que representa a cada cliente del banco
 * 
 * @author EinerZG
 * @version 1.0 12/08/2018
 * 
 */
@Entity
public class Cliente extends Persona {

	private static final long serialVersionUID = 1L;
	/**
	 * lista de peguntas que puede hacer cada cliente
	 */
	@OneToMany(mappedBy = "cliente")
	private List<Pregunta> preguntas;

	public Cliente() {
		super();
	}

}

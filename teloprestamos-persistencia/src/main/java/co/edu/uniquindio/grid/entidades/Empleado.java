package co.edu.uniquindio.grid.entidades;

import co.edu.uniquindio.grid.entidades.Persona;
import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Informacion asociada que representa a cada empleado del banco
 * @author EinerZG
 * @version 1.0 12/08/2018
 */
@Entity
@NamedQueries({@NamedQuery(name= Empleado.LISTAR_EMPLEADO, query="select e from Empleado e")})
public class Empleado extends Persona implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public static final String LISTAR_EMPLEADO = "ListarEmpleados";
	
	/**
	 * salario del empleado
	 */
	private double salario;
	@OneToMany(mappedBy="empleado")
	/**
	 * lsiado de respuestas que da el empleado
	 */
	private List<Respuesta> respuestas;

	public Empleado() {
		super();
	}

	/**
	 * @return the salario
	 */
	public double getSalario() {
		return salario;
	}

	/**
	 * @param salario the salario to set
	 */
	public void setSalario(double salario) {
		this.salario = salario;
	}

	/**
	 * @return the respuestas
	 */
	public List<Respuesta> getRespuestas() {
		return respuestas;
	}

	/**
	 * @param respuestas the respuestas to set
	 */
	public void setRespuestas(List<Respuesta> respuestas) {
		this.respuestas = respuestas;
	}   
	
	
	
}

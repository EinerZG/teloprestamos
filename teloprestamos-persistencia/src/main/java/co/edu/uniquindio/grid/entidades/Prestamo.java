package co.edu.uniquindio.grid.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 * informacion de los tipos de prestamos que se pueden realiza en la entidad bancaria
 * 
 * @author EinerZG
 * @version 1.0 12/08/2018
 */
@Entity
public class Prestamo implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * identificado del prestamo
	 */
	@Id
	@GeneratedValue
	private int id;
	/**
	 * cantidad de dinero que se solicito como prestamo
	 */
	private double dinero;
	/**
	 * fecha en que se realizo el prestamo
	 */
	private Date fecha;
	/**
	 * valor de la cuota que se debe pagar
	 */
	private double valorCuota;
	/**
	 * numero de cuotas que debe de pagar
	 */
	private double numeroCuotas;
	/**
	 * tasa de interes del prestamo
	 */
	private double tasa;
	/**
	 * estado del prestamo
	 */
	private String estado;
	/**
	 * tipos de prestamos que se pueden realizar
	 */
	@Enumerated
	private TipoPrestamos tipo;
	/**
	 * cliente que realizo el prestamo
	 */
	@ManyToOne
	private Persona cliente;
	/**
	 * listado de cuotas pagadas
	 */
	@OneToMany(mappedBy="prestamo")
	private List<Cuota> cuotas;
	/**
	 * bien asociado a un prestamo hipotecario
	 */
	@OneToOne(mappedBy="prestamo")
	private BienRaiz bienRaiz;

	/**
	 * se enuncian los tipos de prestamos que se pueden realizar
	 * @author EinerZG
	 * @version 1.0 12/08/2018
	 */
	private enum TipoPrestamos{
		consumo, personal, educativo, hipotecario, 
	}
	
	public Prestamo() {
		super();
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the dinero
	 */
	public double getDinero() {
		return dinero;
	}

	/**
	 * @param dinero the dinero to set
	 */
	public void setDinero(double dinero) {
		this.dinero = dinero;
	}

	/**
	 * @return the fecha
	 */
	public Date getFecha() {
		return fecha;
	}

	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the valorCuota
	 */
	public double getValorCuota() {
		return valorCuota;
	}

	/**
	 * @param valorCuota the valorCuota to set
	 */
	public void setValorCuota(double valorCuota) {
		this.valorCuota = valorCuota;
	}

	/**
	 * @return the numeroCuotas
	 */
	public double getNumeroCuotas() {
		return numeroCuotas;
	}

	/**
	 * @param numeroCuotas the numeroCuotas to set
	 */
	public void setNumeroCuotas(double numeroCuotas) {
		this.numeroCuotas = numeroCuotas;
	}

	/**
	 * @return the tasa
	 */
	public double getTasa() {
		return tasa;
	}

	/**
	 * @param tasa the tasa to set
	 */
	public void setTasa(double tasa) {
		this.tasa = tasa;
	}

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * @return the tipo
	 */
	public TipoPrestamos getTipo() {
		return tipo;
	}

	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(TipoPrestamos tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return the cliente
	 */
	public Persona getCliente() {
		return cliente;
	}

	/**
	 * @param cliente the cliente to set
	 */
	public void setCliente(Persona cliente) {
		this.cliente = cliente;
	}

	/**
	 * @return the cuotas
	 */
	public List<Cuota> getCuotas() {
		return cuotas;
	}

	/**
	 * @param cuotas the cuotas to set
	 */
	public void setCuotas(List<Cuota> cuotas) {
		this.cuotas = cuotas;
	}

	/**
	 * @return the bienRaiz
	 */
	public BienRaiz getBienRaiz() {
		return bienRaiz;
	}

	/**
	 * @param bienRaiz the bienRaiz to set
	 */
	public void setBienRaiz(BienRaiz bienRaiz) {
		this.bienRaiz = bienRaiz;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Prestamo other = (Prestamo) obj;
		if (id != other.id)
			return false;
		return true;
	}
		
}


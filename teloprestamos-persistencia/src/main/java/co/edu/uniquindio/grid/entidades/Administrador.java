package co.edu.uniquindio.grid.entidades;

import co.edu.uniquindio.grid.entidades.Empleado;
import java.io.Serializable;
import javax.persistence.*;

/**
 * Clase encargada de representar la informacion de los administradores
 * 
 * @author EinerZG
 * @version 1.0 12/08/2018
 */
@NamedQueries({@NamedQuery(name=Administrador.LISTAR_ADMINS,query="select p from Persona p")})
@Entity
public class Administrador extends Empleado implements Serializable {

	public static final String LISTAR_ADMINS = "TodosAdministradores";
	private static final long serialVersionUID = 1L;

	public Administrador() {
		super();
	}
   
}

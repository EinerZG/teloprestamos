package co.edu.uniquindio.grid.entidades;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * 
 * Información del bien raiz que se desea hipotecar
 * @author EinerZG
 * @version 1.0 12/08/2018
 */
@Entity
public class BienRaiz implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 * identificado autogenerado para cada bien raiz
	 */
	@Id
	@GeneratedValue
	private int id;
	/**
	 * direccion del bien raiz
	 */
	private String direccion;
	/**
	 * valor del bien raiz
	 */
	private double valoracion;
	/**
	 * descripcion detallada del bien raiz
	 */
	private String descripcion;
	/**
	 * prestamo con el que esta relacionado
	 */
	@OneToOne
	private Prestamo prestamo;

	public BienRaiz() {
		super();
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the direccion
	 */
	public String getDireccion() {
		return direccion;
	}

	/**
	 * @param direccion the direccion to set
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	/**
	 * @return the valoracion
	 */
	public double getValoracion() {
		return valoracion;
	}

	/**
	 * @param valoracion the valoracion to set
	 */
	public void setValoracion(double valoracion) {
		this.valoracion = valoracion;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the prestamo
	 */
	public Prestamo getPrestamo() {
		return prestamo;
	}

	/**
	 * @param prestamo the prestamo to set
	 */
	public void setPrestamo(Prestamo prestamo) {
		this.prestamo = prestamo;
	}  
	
}

/**
 * 
 */
package co.edu.uniquindio.grid.modelo;

import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import co.edu.uniquindio.grid.entidades.Cliente;
import co.edu.uniquindio.grid.entidades.Persona;
import co.edu.uniquindio.grid.negocio.AdministradorEJBRemote;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Delegado que permite conectar la capa de negocio con la de presentación
 * 
 * @author EinerZG
 * @version 1.0
 */
public class AdministradorDelegado {

	/**
	 * instancia que representa el ejb remoto de administrador
	 */
	private AdministradorEJBRemote administradorEJB;
	/**
	 * permite manejar una unica instancia para le manejo de delegados
	 */
	public static AdministradorDelegado administradorDelegado = instancia();

	/**
	 * constructor para conectar con la capa de negocio
	 */
	private AdministradorDelegado() {
		try {
			administradorEJB = (AdministradorEJBRemote) new InitialContext().lookup(AdministradorEJBRemote.JNDI);
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Permite devolver una unica instancia de delegado
	 * 
	 * @return instancia unica del delegado
	 */
	private static AdministradorDelegado instancia() {

		if (administradorDelegado == null) {
			administradorDelegado = new AdministradorDelegado();
			return administradorDelegado;
		}
		return administradorDelegado;
	}

	/**
	 * pemite registar un nuevo cliente
	 * @param cliente cliente a agregar
	 * @return devuelve true si el cliente fue eliminado
	 */
	public boolean registrarCliente(Cliente cliente) {
		try {
			return administradorEJB.agregarCliente(cliente)!=null;			
		}
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * devuvel la lista de cliente que estan en la base de datos
	 * 
	 * @return todos los clientes
	 */
	public List<Persona> listarClientes() {
		return administradorEJB.listarClientes();
	}
	
	/**
	 * permite eliminar el cliente por cedula
	 * @return cliente si fue eliminado
	 */
	public boolean eliminarCliente(Persona cliente) {
		return (Cliente) administradorEJB.eliminarCliente((Cliente)cliente)!=null;
	}

	/**
	 * genera una lista de clientes observables
	 * @return todos los clientes obsevables
	 */
	public ObservableList<ClienteObservable> listarClientesObservables() {
		List<Persona> clientes = listarClientes();
		ObservableList<ClienteObservable> clientesObservables = FXCollections.observableArrayList();
		for (Persona persona : clientes) {
			clientesObservables.add(new ClienteObservable(persona));
		}
		return clientesObservables;
	}

}

package co.edu.uniquindio.grid.negocio;

import java.util.List;

import javax.ejb.Remote;

import co.edu.uniquindio.grid.entidades.Cliente;
import co.edu.uniquindio.grid.entidades.Empleado;
import co.edu.uniquindio.grid.entidades.Persona;
import co.edu.uniquindio.grid.exceptiones.InformacionRepetidaException;

/**
 * permire generar una capa para dar el acceso solo a los metodos básicos  
 * @author EinerZG
 * @version 1.0
 */
@Remote
public interface AdministradorEJBRemote {

	/**
	 * contexro del EJB
	 */
	String JNDI = "java:global/teloprestamos-ear/teloprestamos-negocio/AdministradorEJB!co.edu.uniquindio.grid.negocio.AdministradorEJBRemote";

	
	public Empleado buscarEmpleado(String cedulaEmpleado);
	
	/**
	 * permite listar todos los cliente del banco
	 * 
	 * @return devuelve todos los clientes del banco
	 */
	public List<Persona> listarClientes();

	/**
	 * pemite actulizar la informacion de un cliente
	 * 
	 * @param cliente cliente a ser actualizado
	 * @return el cliente si se actualiza, null si no
	 */
	public Cliente actualizarCLiente(Cliente cliente);
	
	/**
	 * pasa a un cliente a esto inactivo
	 * @param cliente a inactivar
	 * @return cliente inactivado
	 * @throws Exception si el cliente no esta registrado 
	 */
	public Persona inactivarCliente(Persona cliente) throws Exception;
	
	/**
	 * permite listar todos los clientes inactivos
	 * @return lista de clientes inativos
	 */
	public List<Persona> listarClienteActivos();

	/**
	 * permite eliminar un cliente de la base de datos del banco
	 * 
	 * @param cliente cliente a ser eliminado
	 * @return cliente eliminado o null si no elimina
	 */
	public Cliente eliminarCliente(Cliente cliente);

	/**
	 * Permite agregar un cliente en el banco
	 * 
	 * @param cliente cliente que se desea agregar
	 * @return cliente agregado
	 * @throws InformacionRepetidaException se ejecuta cuando se agrega informacion repetida de clientes
	 */
	public Cliente agregarCliente(Cliente cliente) throws InformacionRepetidaException;
	
	public Empleado agregarEmpleado(Empleado empleado) throws InformacionRepetidaException;



}

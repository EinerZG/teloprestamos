package co.edu.uniquindio.grid.negocio;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import co.edu.uniquindio.grid.entidades.Administrador;

/**
 * EJB encargado de cargar la informacion de la preconfiguracion
 * @author EinerZG
 * @version 1.0
 */
@Singleton
@LocalBean
@Startup
public class SingletonEJB {
	
	@PersistenceContext
	private EntityManager entityManager;

    public SingletonEJB() {}
    
    /**
     * genera un administrador por defecto para la aplicacion
     */
    @PostConstruct
    public void init(){
    	
    	TypedQuery<Administrador> query = entityManager.createNamedQuery(Administrador.LISTAR_ADMINS, Administrador.class);
    	
    	if(query.getResultList().size()==0) {
    		
    		Administrador administrador = new Administrador();
    		administrador.setCedula("10948765");
    		administrador.setNombre("Einer");
    		administrador.setApellido("Zapata");
    		administrador.setClave("1234");
    		administrador.setEmail("ezapata@uniquindio.edu.co");
    		administrador.setEstado("inactivo");
    		entityManager.persist(administrador);
    		
    	}
    	
    	
    }

}

package co.edu.uniquindio.grid.negocio;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import co.edu.uniquindio.grid.entidades.Cliente;
import co.edu.uniquindio.grid.entidades.Pregunta;

/**
 * Session Bean implementation class ClienteEJB
 */
@Stateless
@LocalBean
public class ClienteEJB implements ClienteEJBRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Default constructor.
	 */
	public ClienteEJB() {
	}

	public Pregunta realizarPregunta(Pregunta pregunta) throws Exception {
		
		if (pregunta.getCliente() == null || entityManager.find(Cliente.class, pregunta.getCliente().getCedula()) == null) {
			throw new Exception("El cliente asociado no se ha registrado");
		}
		entityManager.persist(pregunta);
		return pregunta;
	}

	public Cliente buscarCliente(String cedula) {
		return entityManager.find(Cliente.class, cedula);
	}

}

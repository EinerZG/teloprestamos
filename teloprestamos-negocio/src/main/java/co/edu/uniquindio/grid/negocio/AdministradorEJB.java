package co.edu.uniquindio.grid.negocio;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import co.edu.uniquindio.grid.entidades.Cliente;
import co.edu.uniquindio.grid.entidades.Empleado;
import co.edu.uniquindio.grid.entidades.Persona;
import co.edu.uniquindio.grid.entidades.Pregunta;
import co.edu.uniquindio.grid.exceptiones.InformacionRepetidaException;

/**
 * Permite realizar todas las peraciones realcionadas con el administrador
 * 
 * @author EinerZG
 * @version 1.0
 */
@Stateless
@LocalBean
public class AdministradorEJB implements AdministradorEJBRemote {

	/**
	 * permite realizar todas las transacciones con la base de datos
	 */
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * constructor vacio por defecto
	 */
	public AdministradorEJB() {
	}

	public List<Empleado> listarEmpleados() {
		TypedQuery<Empleado> query = entityManager.createNamedQuery(Empleado.LISTAR_EMPLEADO, Empleado.class);
		return query.getResultList();
	}

	public Empleado buscarEmpleado(String cedulaEmpleado) {
		return entityManager.find(Empleado.class, cedulaEmpleado);
	}

	public List<Pregunta> listarPreguntas() {
		TypedQuery<Pregunta> query = entityManager.createNamedQuery(Pregunta.LISTAR_PREGUNTAS, Pregunta.class);
		return query.getResultList();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see co.edu.uniquindio.grid.negocio.AdministradorEJBRemote#listarClientes()
	 */
	public List<Persona> listarClientes() {
		TypedQuery<Persona> query = entityManager.createNamedQuery(Persona.LISTAR_TODOS, Persona.class);
		return query.getResultList();
	}
	
	/*
	 * (non-Javadoc)
	 * @see co.edu.uniquindio.grid.negocio.AdministradorEJBRemote#listarClienteActivos()
	 */
	public List<Persona> listarClienteActivos(){
		TypedQuery<Persona> query = entityManager.createNamedQuery(Persona.LISTAR_CLIENTES_ACTIVOS, Persona.class);
		query.setParameter("estado", "activo");
		return query.getResultList();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * co.edu.uniquindio.grid.negocio.AdministradorEJBRemote#actualizarCLiente(co.
	 * edu.uniquindio.grid.entidades.Cliente)
	 */
	public Cliente actualizarCLiente(Cliente cliente) {
		if (entityManager.find(Cliente.class, cliente.getCedula()) != null) {
			entityManager.merge(cliente);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see co.edu.uniquindio.grid.negocio.AdministradorEJBRemote#inactivarCliente(co.edu.uniquindio.grid.entidades.Persona)
	 */
	public Persona inactivarCliente(Persona cliente) throws Exception {
		if (entityManager.find(Cliente.class, cliente.getCedula()) != null) {
			cliente.setEstado("inactivo");
			entityManager.merge(cliente);
			return cliente;
		}
		else {
			throw new Exception("El cliente no se encuentra registrado");
		}
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * co.edu.uniquindio.grid.negocio.AdministradorEJBRemote#eliminarCliente(co.edu.
	 * uniquindio.grid.entidades.Persona)
	 */
	public Cliente eliminarCliente(Cliente cliente) {
		if (entityManager.find(Persona.class, cliente.getCedula()) != null) {
			if (!entityManager.contains(cliente)) {
				cliente = entityManager.merge(cliente);
			}
			entityManager.remove((Persona) cliente);
			return cliente;
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * co.edu.uniquindio.grid.negocio.AdministradorEJBRemote#agregarCliente(co.edu.
	 * uniquindio.grid.entidades.Cliente)
	 */
	public Cliente agregarCliente(Cliente cliente) throws InformacionRepetidaException {
		if (entityManager.find(Persona.class, cliente.getCedula()) != null) {
			throw new InformacionRepetidaException("Un empleado con esa cedula ya ha sido registrado");
		} else if (buscarPorEmail(cliente.getEmail())) {
			throw new InformacionRepetidaException("Un empleado con ese email ya ha sido registrado");
		}
		cliente.setEstado("activo");
		entityManager.persist(cliente);
		return cliente;
	}

	/**
	 * permite determinar si hay una persona registrada con el mismo email
	 * 
	 * @param email email a ser buscado
	 * @return true si el email se encuentra, false si no
	 */
	private boolean buscarPorEmail(String email) {
		TypedQuery<Persona> query = entityManager.createNamedQuery(Persona.PERSONA_POR_EMAIL, Persona.class);
		query.setParameter("email", email);
		return query.getResultList().size() > 0;
	}

	/**
	 * permite obtener la instcancia de una persona usando sus credenciales
	 * @param cedula cedula del usuario
	 * @param clave calve usada por el usuario
	 * @return instncia de la persona
	 * @throws Exception si el empleado no esta registrado o las credenciales son incorrectas
	 */
	public Persona buscarUsuarioPorCredenciales(String cedula, String clave) throws Exception {
		if (entityManager.find(Persona.class, cedula) == null) {
			throw new Exception("El usuario con esta cedula no se encuentra registrado");
		} else {
			
			TypedQuery<Persona> query  = entityManager.createNamedQuery(Persona.PERSONA_POR_CREDENCIALES, Persona.class);
			query.setParameter("cedula", cedula);
			query.setParameter("clave", clave);
			
			if ( query.getResultList().size() == 0 ) {
				throw new Exception("Los credenciales usadas son incorrectas");
			}
			return query.getSingleResult();
		}

	}

	public Empleado agregarEmpleado(Empleado empleado) throws InformacionRepetidaException {

		if (entityManager.find(Persona.class, empleado.getCedula()) != null) {
			throw new InformacionRepetidaException("Un empleado con esa cedula ya ha sido registrado");
		} else if (buscarPorEmail(empleado.getEmail())) {
			throw new InformacionRepetidaException("Un empleado con ese email ya ha sido registrado");
		}

		entityManager.persist(empleado);
		return empleado;

	}

}

package co.edu.uniquindio.grid.exceptiones;
/**
 * 
 * @author EinerZG
 *
 */
public class InformacionRepetidaException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public InformacionRepetidaException(String mensaje) {
		super(mensaje);
	}

}
